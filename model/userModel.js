/**
 * @author wangxin45@jd.com
 * @date 2017/4/10
 */
// 创建数据库
var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database('users_database');
db.run("CREATE TABLE if not exists user (userName TEXT, userLabel NUMERIC)");

// 读取数据中key和value
export function getUserLabel(userName, callback){
    db.serialize(function(){
        db.all("SELECT userLabel FROM user where userName = ?", userName , function(err, rows) {
                callback(err, rows);
        });
    });
}

// 写入数据库key和value
export function setUserLabel(userName, userLabel, callback){
    db.serialize(function(){
        db.run("delete from user where userName = ?", userName, function(e) {
            if(e == null) {
                db.run("INSERT INTO user VALUES (?,?)", userName, userLabel , function(e) {
                    callback(e);
                });
            } else {
                callback(e);
            }
        });

    });
}


