var url = require("url");
var fs = require("fs");
var path = require("path");

var express = require('express');
import {getUserLabel} from "../model/userModel";
import {setUserLabel} from "../model/userModel";
var router = express.Router();

router.post('/get', function (req, res, next) {
    res.set('Content-Type', 'application/json');
    try {
        var arg = req.body;
        var key = arg.key;
        if (!key) {
            res.send({'resultCode': -1, resultData: {}, 'resultMsg': '请使用{"key":xxx}的形式传递查询参数, 传{"key":"-1"}返回所有数据'});
            return;
        }

        getUserLabel(key, function(err, rows){
            if(err != null) {
                res.send({'resultCode': -1, resultData: {}, 'resultMsg': err.message});
            } else {
                if(rows != null && rows.length > 0) {
                    res.send({
                        'resultCode': 200,
                        resultData: rows[0].userLabel,
                        'resultMsg': '获取key为' + key + '的数据成功'
                    });
                } else {
                    // 随机生成一个userLabel, 并存储在数据库中
                    var randomUserLabel = new Date().getTime();
                    setUserLabel(key, randomUserLabel, function(e){
                        if(e != null) {
                            throw e.message;
                        } else {
                            res.send({'resultCode': 200, resultData: randomUserLabel, 'resultMsg': '随机生成了结果!'});
                        }
                    });
                }
            }
        })
    } catch (e) {
        res.send({'resultCode': -1, resultData: {}, 'resultMsg': e.message});
    }
});

module.exports = router;
